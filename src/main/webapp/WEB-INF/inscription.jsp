<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<h1>Inscription</h1>

<form method="post">

	<input type="hidden" name="action" value="inscrire" />

	<div class="input-field">
		<i class="material-icons prefix">account_circle</i> 
		<input id="login" type="text" class="validate" name="LOGIN_UTILISATEUR"> 
		<label for="login">Votre login</label>	
	
	</div>
	<div class="input-field s6">
		<i class="material-icons prefix">account_circle</i> 
		<input id="mdp" type="text" class="validate" name="MOT_DE_PASSE"> 
		<label for="mdp">Votre mot de passe</label>
	</div>
	<div class="input-field s6">
		<i class="material-icons prefix">lock</i> 
		<input id="mdpconfirm" type="text" class="validate" name="MOT_DE_PASSE_COMFIRME"> 
		<label for="mdpconfirm">Confirmez votre mot de passe</label>
	</div>
	<div class="input-field s6">
		<i class="material-icons prefix">lock</i> 
		<input id="nom" type="text" class="validate" name="NOM_UTILISATEUR"> 
		<label for="nom">Nom</label>
	</div>
	<div class="input-field s6">
		<i class="material-icons prefix">edit</i> 
		<input id="prenom" type="text" class="validate" name="PRENOM_UTILISATEUR"> 
		<label for="prenom">Prenom</label>
	</div>
	<div class="input-field s6">
		<i class="material-icons prefix">edit</i> 
		<input id="age" type="text" class="validate" name="AGE_UTILISATEUR"> 
		<label for="age">Age</label>
	</div>
	<div class="input-field s6">
		<i class="material-icons prefix">edit</i> 
		<input id="mail" type="text" class="validate" name="CODE_POSTAL"> 
		<label for="mail">Code Postal</label>
	</div>
	<div class="input-field s6">
		<i class="material-icons prefix">phone</i> 
		<input id="tel" type="text" class="validate" name="TELEPHONE_UTILISATEUR"> 
		<label for="tel">Telephone</label>
	</div>
	<center><button class="btn waves-effect waves-light" type="submit"> 
		S'inscrire <i class="material-icons right">send</i>
	</button></center>
</form>



