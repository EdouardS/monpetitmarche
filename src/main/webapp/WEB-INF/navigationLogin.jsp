<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>


<nav>
	<div class="nav-wrapper  green darken-2">

		<a class="brand-logo">Mon petit marché</a>
		<div>
		
		<form class="col s12" method="post" action="login" >
		
			<div class="row">
			<ul class="right hide-on-med-and-down">
		
			<li>
				
				<div class="input-field col s6">
					<input placeholder="Login" id="login" type="text" class="validate" name="LOGIN_UTILISATEUR">
				</div>
				<div class="input-field col s6">
					<input placeholder="Mot de passe" id="mdp" type="password" class="validate" name="MOT_DE_PASSE" >
				</div></li>
			
			<li>
				<button  class="waves-effect waves-light btn  green darken-3" type="submit">Se connecter</button></li>
			</ul>
			</div>
		</form>
		</div>
	</div>
</nav>