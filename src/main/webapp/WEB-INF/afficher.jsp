<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="row">
	<!-- Pour toute les cartes de la liste -->
	<c:forEach items="${requestScope['Produit']}" var="produit">
		<div class="col">
			<div class="card s12 m7 z-depth-5">

				<!-- Affichage du contenu (non deroulant) -->
				<div>
					<span class="card-title">${produit.nom}</span>
					<ol>
						<li>Type : ${produit.type}</li>
						<li>Prix : ${produit.prixEuro},${produit.prixCentime}€/kg</li>
<%-- 						<li>Producteur : ${produit.utilisateurDuProduit.nom}</li> --%>
						<c:if test="${panierUtilisateur.contains(produit.id)}">
							<li><a href="ToggleProduitPanier?idProduit=${produit.id }">Supprimer
									du panier</a></li>
						</c:if>
						<c:if
							test="${empty panierUtilisateur or not panierUtilisateur.contains(produit.id) }">
							<li><a href="ToggleProduitPanier?idProduit=${produit.id }">Ajouter
									au panier</a></li>
						</c:if>
					</ol>
				</div>
			</div>
		</div>
	</c:forEach>
</div>
