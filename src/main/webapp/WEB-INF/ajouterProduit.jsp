<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Ajouter Produit</title>
</head>
<body>

	<div class="row">
		<form class="col s12" action="ajouterProduit" method="post">
			<div class="row">
				<div class="input-field col s6">
					<input id="NOM_PRODUIT" type="text" name="NOM_PRODUIT"> <label
						for="NOM_PRODUIT">Nom</label>
				</div>
			</div>
			<div class="row">
				<div class="input-field col s12">
					<select class="browser-default" name="TYPE_PRODUIT">
						<option value="" disabled selected>Choisir votre type</option>
						<option value="1">Légume</option>
						<option value="2">Fruit</option>
					</select>
				</div>
			</div>

			<div class="row">
				<div class="input-field col s12">
					<input id="PRIX_PRODUIT" type="text" name="PRIX_PRODUIT"> <label
						for="PRIX_PRODUIT">Prix €/kg</label>
				</div>
			</div>
			<button class="btn waves-effect waves-light" type="submit"
				name="action">
				Ajouter <i class="material-icons right">add</i>
			</button>
		</form>
	</div>
</body>
</html>