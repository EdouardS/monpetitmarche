<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>


<ul id="dropdown1" class="dropdown-content">
  <li><a href="#!">Liste Achat</a></li>
  <li><a href="afficherPanier">Panier</a></li>
</ul>
<u1 id="dropdown2" class="dropdown-content">
  <li><a href="#!">Mes produit</a></li>
  <li><a href="editproduit">Ajouter un produit</a></li>
  
</u1>

<%
	String nomUtilisateur = (String) request.getSession().getAttribute("LOGIN_UTILISATEUR");
%>

<nav>
  <div class="nav-wrapper green darken-2">
    <a href="#!" class="brand-logo">Mon petit marché</a>
    <ul class="right hide-on-med-and-down"> 
    	<li><%= nomUtilisateur	 %></li>
        <li><a class="dropdown-button" href="#!" data-activates="dropdown2">Vente<i class="material-icons right">arrow_drop_down</i></a></li>
        <li><a class="dropdown-button" href="#!" data-activates="dropdown1">Achat<i class="material-icons right">arrow_drop_down</i></a></li>
        <li><a class="waves-effect waves-light btn  green darken-3" href="logout" type="submit">Se déconnecter</a></li></li>
    </ul>
    
  </div>
</nav>
