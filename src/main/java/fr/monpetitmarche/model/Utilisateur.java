package fr.monpetitmarche.model;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

@Entity
public class Utilisateur
{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@NotNull
	private String nom;

	@NotNull
	private String prenom;

	@NotNull
	private String login;

	@NotNull
	private String motDePasse;

	@NotNull
	private Integer age;

	@NotNull
	private Integer codePostal;

	@NotNull
	private Integer telephone;
	
	@OneToMany(mappedBy = "utilisateurDuProduit")
	Set<Produit> Produits;

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public String getNom()
	{
		return nom;
	}

	public void setNom(String nom)
	{
		this.nom = nom;
	}

	public String getPrenom()
	{
		return prenom;
	}

	public void setPrenom(String prenom)
	{
		this.prenom = prenom;
	}

	public String getLogin()
	{
		return login;
	}

	public void setLogin(String login)
	{
		this.login = login;
	}

	public String getMotDePasse()
	{
		return motDePasse;
	}

	public void setMotDePasse(String motDePasse)
	{
		this.motDePasse = motDePasse;
	}

	public Integer getAge()
	{
		return age;
	}

	public void setAge(Integer age)
	{
		this.age = age;
	}

	public Integer getCodePostal()
	{
		return codePostal;
	}

	public void setCodePostal(Integer codePostal)
	{
		this.codePostal = codePostal;
	}

	public Integer getTelephone()
	{
		return telephone;
	}

	public void setTelephone(Integer telephone)
	{
		this.telephone = telephone;
	}

}
