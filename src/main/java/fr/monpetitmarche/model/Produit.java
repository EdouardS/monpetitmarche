package fr.monpetitmarche.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Entity
public class Produit
{
	@NotNull
	private String nom;
	@NotNull
	private String type;
	@NotNull
	private Integer prixEuro;
	private Integer prixCentime;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name="utilisateur_id")
	private Utilisateur utilisateurDuProduit;

	
	public Produit(String nom, String type, Integer prixEuro, Integer prixCentime, Utilisateur utilisateurDuProduit)
	{
		this.nom = nom;
		this.type = type;
		this.prixEuro = prixEuro;
		this.prixCentime = prixCentime;
		this.utilisateurDuProduit = utilisateurDuProduit;
	}
	
	public Produit()
	{

	}
	public String getType()
	{
		return type;
	}

	public void setType(String type)
	{
		this.type = type;
	}

	public String getNom()
	{
		return nom;
	}

	public void setNom(String nom)
	{
		this.nom = nom;
	}

	public Integer getPrixEuro()
	{
		return prixEuro;
	}

	public void setPrixEuro(Integer prixEuro)
	{
		this.prixEuro = prixEuro;
	}

	public Integer getPrixCentime()
	{
		return prixCentime;
	}

	public void setPrixCentime(Integer prixCentime)
	{
		this.prixCentime = prixCentime;
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public Utilisateur getUtilisateurDuProduit()
	{
		return utilisateurDuProduit;
	}

	public void setUtilisateurDuProduit(Utilisateur utilisateurDuProduit)
	{
		this.utilisateurDuProduit = utilisateurDuProduit;
	}


}
