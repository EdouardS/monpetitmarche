package fr.monpetitmarche.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.monpetitmarche.model.Produit;

//TEST

@Stateless
public class ProduitDao
{

	@PersistenceContext(name = "MonPetitMarcheJPA")
	private EntityManager em;

	public void ajouterProduit(Produit produit)
	{
		em.persist(produit);
	}

	public void supprimerProduit(Integer idProduit)
	{
		Produit produit = em.find(Produit.class, idProduit);
		produit = em.merge(produit);
		em.remove(produit);
	}

	public Produit trouverProduit(Integer idProduit)
	{
		return em.find(Produit.class, idProduit);
	}

	public Produit majProduit(Produit produit)
	{
		return em.merge(produit);

	}

	/**
	 * Trouve un utilisateur avec le nom
	 * 
	 * @param nomProduit
	 */
	public Produit touverProduitParNom(String nomProduit)
	{
		Query query = em.createQuery("from Produit p where p.nom = nomProduit");
		Produit produit;
		try
		{
			produit = (Produit) query.getSingleResult();
		} catch (NoResultException e)
		{
			return null;
		}
		return produit;
	}

	public List<Produit> trouverTousProduits()
	{
		Query query = em.createQuery("from Produit");
		@SuppressWarnings("unchecked")
		List<Produit> result = (List<Produit>) query.getResultList();
		return result;
	}
}
