package fr.monpetitmarche.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import fr.monpetitmarche.model.Utilisateur;

@Stateless
public class UtilisateurDao
{

	@PersistenceContext(name = "MonPetitMarcheJPA")
	EntityManager em;

	public Utilisateur getUtilisateur()
	{
		@SuppressWarnings("unchecked")
		List<Utilisateur> result = (List<Utilisateur>) em.createQuery("from Utilisateur").getResultList();
		return (Utilisateur) result;

	}

	public void ajouterUtilisateur(Utilisateur utilisateur)
	{
		em.persist(utilisateur);
	}

	public void modifierUtilisateur(Utilisateur utilisateur)
	{
		em.merge(utilisateur);
	}

	public void supprimerUtilisateur(Utilisateur utilisateur)
	{
		em.remove(utilisateur);
	}

	public Utilisateur login(String login, String motDePasse)
	{
		Query query = em.createQuery("from Utilisateur u where u.login=:login and u.motDePasse=:motDePasse");
		query.setParameter("login", login);
		query.setParameter("motDePasse", motDePasse);

		try
		{
			Utilisateur u = (Utilisateur) query.getSingleResult();
			return u;
		} catch (NoResultException e)
		{
			return null;
		}
	}

	public Utilisateur trouverUtilisateurParLogin(String login)
	{
		Query query = em.createQuery("from Utilisateur u where u.login=:login");
		query.setParameter("login", login);

		try
		{
			Utilisateur u = (Utilisateur) query.getSingleResult();
			return u;
		} catch (NoResultException e)
		{
			return null;
		}

	}

}
