package fr.monpetitmarche.Servlet;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.monpetitmarche.dao.ProduitDao;
import fr.monpetitmarche.model.Produit;

public class AfficherAchatServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;

	@EJB
	private ProduitDao produitDao;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		List<Produit> listeProduit = produitDao.trouverTousProduits();

		Rendu.pageAffichageProduit(listeProduit, false, false, true, getServletContext(), request, response);
	}
}
