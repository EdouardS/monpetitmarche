package fr.monpetitmarche.Servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.monpetitmarche.dao.UtilisateurDao;
import fr.monpetitmarche.model.Utilisateur;

public class InscriptionServlet extends HttpServlet
{

	private static final long serialVersionUID = 1L;

	@EJB
	private UtilisateurDao utilisateurDao;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{

		Rendu.pageBienvenue(getServletContext(), request, response);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{

		// Recuperation des parametres utilisateur
		String nomUtilisateur = req.getParameter("NOM_UTILISATEUR");
		String prenomUtilisateur = req.getParameter("PRENOM_UTILISATEUR");
		String loginUtilisateur = req.getParameter("LOGIN_UTILISATEUR");
		String motDePasse = req.getParameter("MOT_DE_PASSE");
		String motDePasseComfirme = req.getParameter("MOT_DE_PASSE_COMFIRME");
		Integer ageUtilisateur=0;
		Integer codePostal=0;
		Integer telephoneUtilisateur=0;
		List<Boolean> listValider = new ArrayList<>();
		
		if(isNumber(req.getParameter("AGE_UTILISATEUR")))
		{
			ageUtilisateur = Integer.parseInt(req.getParameter("AGE_UTILISATEUR"));
			listValider.add(validationAge(ageUtilisateur));
		}
		else
			listValider.add(false);
		
		if(isNumber(req.getParameter("CODE_POSTAL")))
		{
			codePostal = Integer.parseInt(req.getParameter("CODE_POSTAL"));
			listValider.add(validationCodePostal(codePostal));
		}
		else
			listValider.add(false);
		
		if(isNumber(req.getParameter("TELEPHONE_UTILISATEUR")))
		{
			telephoneUtilisateur = Integer.parseInt(req.getParameter("TELEPHONE_UTILISATEUR"));
			listValider.add(validationTelephone(telephoneUtilisateur));
		}
		else
			listValider.add(false);

		// Si aucuns parametres faux alors on cree et on ajoute l'utilisateur
		if (!validerUtilisateur(loginUtilisateur, nomUtilisateur, prenomUtilisateur, motDePasse,
				motDePasseComfirme).contains(false))
		{
			utilisateurDao.ajouterUtilisateur(creerUtilisateurs(loginUtilisateur, nomUtilisateur, prenomUtilisateur,
					 ageUtilisateur, motDePasse, motDePasseComfirme, codePostal, telephoneUtilisateur));
			
			Rendu.pageAccueil(creerUtilisateurs(loginUtilisateur, nomUtilisateur, prenomUtilisateur,
					 ageUtilisateur, motDePasse, motDePasseComfirme, codePostal, telephoneUtilisateur),getServletContext(), req, resp);

		} else
		{
			Rendu.pageBienvenue(getServletContext(), req, resp);
		}
	}

	private boolean isNumber(String S)
	{
		try
		{
			Integer.parseInt(S);
			return true;
		} catch (Exception e)
		{
			return false;
		}
	}
	
	private Utilisateur creerUtilisateurs(String loginUtilisateur, String nomUtilisateur, String prenomUtilisateur,
			Integer ageUtilisateur, String motDePasse, String motDePasseComfirme, Integer codePostal,
			Integer telephoneUtilisateur)
	{
		Utilisateur utilisateur = new Utilisateur();
		utilisateur.setLogin(loginUtilisateur);
		utilisateur.setPrenom(prenomUtilisateur);
		utilisateur.setNom(nomUtilisateur);
		utilisateur.setAge(ageUtilisateur);
		utilisateur.setMotDePasse(motDePasseComfirme);
		utilisateur.setCodePostal(codePostal);
		utilisateur.setTelephone(telephoneUtilisateur);
		return utilisateur;
	}

	private List<Boolean> validerUtilisateur(String loginUtilisateur, String nomUtilisateur, String prenomUtilisateur,
			String motDePasse, String motDePasseComfirme)
	{
		List<Boolean> validation = new ArrayList<Boolean>();

		validation.add(validationNoms(loginUtilisateur));
		validation.add(validationNoms(nomUtilisateur));
		validation.add(validationNoms(prenomUtilisateur));
		validation.add(validationPassword(motDePasse, motDePasseComfirme));

		return validation;
	}

	private boolean validationPassword(String motDePasse, String motDePasseComfirme)
	{
		if (motDePasse.equals(motDePasseComfirme) && !motDePasse.equals(""))
		{
			return true;
		} else
		{
			return false;
		}
	}

	private boolean validationAge(int ageUtilisateur)
	{
		if (ageUtilisateur >= 18)
		{
			return true;
		} else
		{
			return false;
		}
	}

	private boolean validationNoms(String name)
	{
		if (!name.equals(""))
		{
			return true;
		} else
		{
			return false;
		}
	}

	private boolean validationCodePostal(Integer codePostal)
	{
		if (codePostal != null && String.valueOf(codePostal).length() == 5)
		{
			return true;
		} else
		{
			return false;
		}
	}

	private boolean validationTelephone(Integer telephone)
	{
		if (telephone != null && String.valueOf(telephone).length() != 10)
		{
			return true;
		} else
		{
			return false;
		}
	}
}
