package fr.monpetitmarche.Servlet.action;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.monpetitmarche.Servlet.Rendu;
import fr.monpetitmarche.dao.ProduitDao;
import fr.monpetitmarche.model.Produit;

public class AjouterProduitServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;
	@EJB
	ProduitDao dao;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		Rendu.pageAjoutProduit(getServletContext(), req, resp);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{

		String nomProduit = request.getParameter("NOM_PRODUIT");
		Integer typeProduit = Integer.parseInt(request.getParameter("TYPE_PRODUIT"));
		String prixProduit = request.getParameter("PRIX_PRODUIT");

		String produitModel[] =
		{ "Legume", "Fruit" };

		// validations

		boolean nomOk = validerNom(nomProduit);
		boolean typeOk = validerType(typeProduit);
		boolean prixOk = validerPrix(prixProduit);

		if (nomOk != false && typeOk != false && prixOk != false)
		{
			try
			{
				Produit produit = creerNouveauProduit(nomProduit, typeProduit, prixProduit, produitModel);
				dao.ajouterProduit(produit);

			} catch (Exception e)
			{
				// TODO: handle exception
			}
			
			//REDIRECTION A MODIFIER
			response.sendRedirect("ajouterProduit");
		} else
		{
			response.sendRedirect("ajouterProduit");
		}

	}

	private Produit creerNouveauProduit(String nomProduit, Integer typeProduit, String prixProduit,
			String[] produitModel)
	{
		Produit produit = new Produit();
		produit.setNom(nomProduit);
		produit.setType(produitModel[typeProduit - 1]);
		if (prixProduit != null)
		{
			int prixEuro = recupererPartieEuros(prixProduit);
			int prixCentimes = recupererPartieCentimes(prixProduit);
			// int prixEuro = Character.getNumericValue(prixProduit.charAt(0));
			// int prixCentimes =
			// Character.getNumericValue(prixProduit.charAt(2))
			// + Character.getNumericValue(prixProduit.charAt(3));

			produit.setPrixEuro(prixEuro);
			produit.setPrixCentime(prixCentimes);
		}

		return produit;
	}

	private boolean validerPrix(String prixProduit)
	{
		if (prixProduit != null && prixProduit.contains("."))
		{
			return true;
		}

		return false;
	}

	private boolean validerType(Integer typeProduit)
	{
		if (typeProduit != null && (typeProduit == 1 || typeProduit == 2))
		{
			return true;
		}
		return false;
	}

	private boolean validerNom(String nomProduit)
	{
		if (nomProduit != null)
		{
			return true;

		}
		return false;
	}

	private int recupererPartieEuros(String prixProduit)
	{

		String splitVar[] = prixProduit.split("\\.");

		return Integer.parseInt(splitVar[0]);
	}

	private int recupererPartieCentimes(String prixProduit)
	{

		String splitVar[] = prixProduit.split("\\.");

		return Integer.parseInt(splitVar[1]);
	}

}
