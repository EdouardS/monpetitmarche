package fr.monpetitmarche.Servlet;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import antlr.ParserSharedInputState;
import fr.monpetitmarche.dao.ProduitDao;
import fr.monpetitmarche.dao.UtilisateurDao;
import fr.monpetitmarche.model.Produit;
import fr.monpetitmarche.model.Utilisateur;

public class EditProduitServlet extends HttpServlet

{
	private static final long serialVersionUID = 1L;

	@EJB
	ProduitDao produitDao;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		Rendu.pageAjoutProduit(getServletContext(), request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		Produit produit = null;

		if (request.getParameter("ID_PRODUIT") != null)
		{

			Integer id = Integer.parseInt(request.getParameter("ID_PRODUIT"));

			produit = produitDao.trouverProduit((id));

			if (produit == null)
			{
				response.sendRedirect("affichageProduitVente");
				return;
			}
		} else
		{
			produit = new Produit();
			produitDao.ajouterProduit(produit);
		}
		
		String nom = request.getParameter("NOM_PRODUIT");
		String type = request.getParameter("TYPE_PRODUIT");
		String prixProduit = request.getParameter("PRIX_PRODUIT");
		String partsPrix[] = prixProduit.split("\\.");
		Integer prixEuro = Integer.parseInt(partsPrix[0]);
		Integer prixCentime = Integer.parseInt(partsPrix[1]);

		Utilisateur utilisateur = trouverUtilisateur(request);

		modificationDuProduit(produit, nom, type, prixEuro, prixCentime, utilisateur);

		produitDao.majProduit(produit);

		response.sendRedirect("affichageProduitVente");
	}

	public void modificationDuProduit(Produit produit, String nom, String type, Integer prixEuro, Integer prixCentime,
			Utilisateur utilisateur)
	{
		produit.setNom(nom);
		produit.setType(type);
		produit.setPrixEuro(prixEuro);
		produit.setPrixCentime(prixCentime);
		produit.setUtilisateurDuProduit(utilisateur);
	}

	@EJB
	UtilisateurDao utilisateurDao;

	public Utilisateur trouverUtilisateur(HttpServletRequest request)
	{
		String loginUtilisateur = (String) request.getSession().getAttribute("LOGIN");
		Utilisateur utilisateur = utilisateurDao.trouverUtilisateurParLogin(loginUtilisateur);
		return utilisateur;
	}

}
