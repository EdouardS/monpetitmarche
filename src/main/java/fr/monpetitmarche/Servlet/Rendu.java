package fr.monpetitmarche.Servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.monpetitmarche.model.Produit;
import fr.monpetitmarche.model.Utilisateur;

public class Rendu
{
		
	public static void pageAffichageProduit( List<Produit> listeProduit, boolean mesProduits, boolean monPanier, boolean tousProduit, ServletContext context, HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException
	{
		request.setAttribute("mesProduits", mesProduits);
		request.setAttribute("monPanier", monPanier);
		request.setAttribute("tousProduit", tousProduit);
		request.setAttribute( "Produit", listeProduit );
		pagePrincipale("navigationUtilisateur.jsp", "Bienvenue", "/WEB-INF/afficher.jsp", context, request, response );
	}
	
	public static void pageBienvenue(ServletContext servletContext, HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException 
	{
		
		pagePrincipale("navigationLogin.jsp", "Bienvenue", "/WEB-INF/inscription.jsp", servletContext, req, resp);
	}
	
	public static void pageAccueil(Utilisateur utilisateur, ServletContext servletContext, HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException 
	{
		req.getSession().setAttribute("UTILISATEUR", utilisateur);
		pagePrincipale("navigationUtilisateur.jsp", "Bienvenue", "/WEB-INF/accueil.jsp", servletContext, req, resp);
	}
	
	
	public static void pagePrincipale( String navigation, String title, String contentJsp, ServletContext context, HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException
	{
		request.setAttribute( "pageTitle", title );
		request.setAttribute( "contentJsp", contentJsp );
		request.setAttribute( "navigation", navigation );

		RequestDispatcher dispatcher = context.getRequestDispatcher( "/WEB-INF/body.jsp" );

		dispatcher.forward( request, response );
	}
	
	public static void pageAjoutProduit(ServletContext context, HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
	{
		pagePrincipale("navigationUtilisateur.jsp", "Ajout de produit", "/WEB-INF/ajouterProduit.jsp", context, request,
				response);
	}
}

