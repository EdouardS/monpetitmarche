package fr.monpetitmarche.Servlet;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ToggleProduitPanier
 */
public class ToggleProduitPanier extends HttpServlet
{
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		String id = request.getParameter("idProduit");
		

		if (id != null)
		{
			@SuppressWarnings("unchecked")
			Set<String> panier = (Set<String>) request.getSession().getAttribute("panierUtilisateur");
			if (panier == null)
			{
				panier = new HashSet<>();
				request.getSession().setAttribute("panierUtilisateur", panier);
			}

			if (panier.contains(id))
				panier.remove(id);
			else
				panier.add(id);
		}

		String referer = request.getHeader("referer");
		if (referer == null)
			referer = "affichageProduitVente";

		response.sendRedirect(referer);
	}

}
