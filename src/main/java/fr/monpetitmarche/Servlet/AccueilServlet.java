package fr.monpetitmarche.Servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.monpetitmarche.model.Utilisateur;

public class AccueilServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		Utilisateur utilisateur = null;
		Rendu.pageAccueil(utilisateur, getServletContext(), req, resp);
	}

}
