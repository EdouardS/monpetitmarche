package fr.monpetitmarche.Servlet;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.monpetitmarche.dao.ProduitDao;
import fr.monpetitmarche.dao.UtilisateurDao;
import fr.monpetitmarche.model.Produit;
import fr.monpetitmarche.model.Utilisateur;

public class LoginServlet extends HttpServlet
{
	@EJB
	UtilisateurDao dao;

	@EJB
	private ProduitDao produitDao;

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		List<Produit> listeProduit = produitDao.trouverTousProduits();
		Rendu.pageAffichageProduit(listeProduit, false, false, true, getServletContext(), req, resp);

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{
		// Recuperation des parametres de l'utilisateur

		String LOGIN_UTILISATEUR = req.getParameter("LOGIN_UTILISATEUR");
		String MOT_DE_PASSE = req.getParameter("MOT_DE_PASSE");

		// Verification des données de l'utilisateur
		if (verifierIdentifiantsUtilisateurs(LOGIN_UTILISATEUR, MOT_DE_PASSE))
		{
			req.getSession().setAttribute("LOGIN_UTILISATEUR", LOGIN_UTILISATEUR);
			List<Produit> listeProduit = produitDao.trouverTousProduits();
			Rendu.pageAffichageProduit(listeProduit, false, false, true, getServletContext(), req, resp);
		} else
		{
			Rendu.pageBienvenue(getServletContext(), req, resp);
		}

	}

	private boolean verifierIdentifiantsUtilisateurs(String loginUtilisateur, String motDePasse)
	{

		Utilisateur utilisateur = dao.trouverUtilisateurParLogin(loginUtilisateur);
		if (utilisateur != null && utilisateur.getMotDePasse().equals(motDePasse))
		{
			return true;
		} else
		{
			return false;
		}

	}

}
