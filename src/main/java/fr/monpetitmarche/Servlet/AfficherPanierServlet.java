package fr.monpetitmarche.Servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.monpetitmarche.dao.ProduitDao;
import fr.monpetitmarche.model.Produit;

public class AfficherPanierServlet extends HttpServlet
{
	private static final long serialVersionUID = 1L;

	@EJB
	ProduitDao produitDao;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		@SuppressWarnings("unchecked")

		Set<String> idProduit = (Set<String>) request.getSession().getAttribute("panierUtilisateur");
		List<Produit> produits = new ArrayList<>();
		for (String id : idProduit)
		{
			int idInt = Integer.parseInt(id);
			produits.add(produitDao.trouverProduit(idInt));
		}
		Rendu.pageAffichageProduit(produits, false, true, false, getServletContext(), request, response);

	}

}
