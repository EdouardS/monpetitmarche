package tests;

import fr.monpetitmarche.model.Produit;
import junit.framework.TestCase;

public class ProduitTest extends TestCase
{
	private Produit p;

	protected void setUp() throws Exception
	{
		p = new Produit();
	}

	public void testTypeProduit() throws Exception
	{
		p.setType("FRUIT");
		assertEquals("FRUIT", p.getType());
	}
	
	public void testNomProduit() throws Exception
	{
		p.setNom("tomate");
		assertEquals("tomate", p.getNom());
	}
	

}
